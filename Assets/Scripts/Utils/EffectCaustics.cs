﻿using UnityEngine;
using System.Collections.Generic;

public class EffectCaustics : MonoBehaviour
{
    public Texture firstTexture;
    public bool animationCaustics;
    public List<Texture> causticTex;

    private int nTex = 0;

	void Start ()
    {
        if (animationCaustics)
            for (int n = 1; n < 257; n++) causticTex.Add(Resources.Load("Caustics/_" + n.ToString("000")) as Texture);
	}
	
	void Update ()
    {
        if (!animationCaustics) return;

        GetComponent<Projector>().material.SetTexture("_ShadowTex", causticTex[nTex]);
        nTex += 3;
        if (nTex >= causticTex.Count) nTex = 0;
	}

    private void OnApplicationQuit()
    {
        GetComponent<Projector>().material.SetTexture("_ShadowTex", firstTexture);
    }
}
